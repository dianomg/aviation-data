import Foundation
import MapKit

struct MapRegion {
    
    let width: Double
    let height: Double
    
    var inMiles: MapRegion {
        let width = Measurement(value: self.width, unit: UnitLength.meters).converted(to: UnitLength.miles)
        let height = Measurement(value: self.height, unit: UnitLength.meters).converted(to: UnitLength.miles)
        return MapRegion(width: width.value, height: height.value)
    }
    
    init(region: MKCoordinateRegion) {
        let deltaLatitude = region.span.latitudeDelta
        let deltaLongitude = region.span.longitudeDelta
        let latitudeCircumference = 40075160 * cos(region.center.latitude * .pi / 180)
        
        self.width = deltaLongitude * latitudeCircumference / 360
        self.height = deltaLatitude * 40008000 / 360
    }
    
    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }
}
