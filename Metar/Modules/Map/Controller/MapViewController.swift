import UIKit
import MapKit
import SnapKit

class MapViewController: UIViewController {
    
    private let viewModel: MapViewModel
    
    private let hudView = MapHudView()
    private lazy var mapView = MapView(viewModel: viewModel)
    private let pointsView = MapPointsView()
    
    private let switchView = SwitchView()
    private let loadingView = LoadingView(text: "Loading...")
    
    init(viewModel: MapViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        viewModel.controller = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        
        createViews()
        setupBindings()
        
        viewModel.loadData()
        
        updateMapModeUI(animated: false)
        updateRouteUI(event: .reload)
    }
    
    private func createViews() {
        view.addSubview(mapView)
        view.addSubview(hudView)
        view.addSubview(pointsView)
        view.addSubview(switchView)
        view.addSubview(loadingView)
        
        mapView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        switchView.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().inset(32)
        }
        
        loadingView.snp.makeConstraints { (make) in
            make.right.equalTo(hudView.snp.right)
            make.bottom.equalTo(hudView.snp.top).offset(-16)
        }
    }
    
    private func setupBindings() {
        viewModel.viewModelChanged = { [weak self] event in
            switch event {
            case .reload:
                self?.updateUI(animated: true)
            case .updateCircles:
                self?.updateCirclesUI()
            case .updateHud:
                self?.updateHudUI()
            case .updateMapMode:
                self?.updateMapModeUI(animated: true)
            case .updateLoading:
                self?.updateLoadingUI()
            }
        }
        
        viewModel.pointsVm.viewModelChanged = { [weak self] event in
            self?.updateRouteUI(event: event)
        }
        
        switchView.switched = { [weak self] in self?.viewModel.mapModeSwitched() }
    }
    
    private func updateUI(animated: Bool) {
        updateCirclesUI()
        updateHudUI()
    }
    
    private func updateRouteUI(event: MapPointsViewModel.ViewEvent) {
        switch event {
        case .append, .reload:
            mapView.updatePoints(items: viewModel.pointsVm.items)
        default:
            break
        }
        pointsView.setup(viewModel: viewModel, event: event)
    }
    
    private func updateCirclesUI() {
        mapView.updateCircles(items: viewModel.circleItems)
    }
    
    private func updateHudUI() {
        hudView.setup(viewModel: viewModel.hudVm)
    }
    
    private func updateMapModeUI(animated: Bool) {
        pointsView.update(mapMode: viewModel.mapMode, animated: animated)
    }
    
    private func updateLoadingUI() {
        viewModel.isLoading ? loadingView.show() : loadingView.hide()
    }
    
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
//        hudView.updateSafeAreaInsets(view.safeAreaInsets)
        setupHudViewConstraints(view.safeAreaInsets)
        setupPointsViewConstraints(view.safeAreaInsets)
    }
    
    private func setupHudViewConstraints(_ inset: UIEdgeInsets) {
        hudView.snp.remakeConstraints { (make) in
            make.left.equalToSuperview().inset(16 + inset.left)
            make.right.equalToSuperview().inset(16 + inset.right)
            make.bottom.equalToSuperview().inset(16 + inset.bottom)
            make.height.equalTo(58)
        }
    }
    
    private func setupPointsViewConstraints(_ inset: UIEdgeInsets) {
        pointsView.snp.remakeConstraints { (make) in
            if UIDevice.current.userInterfaceIdiom == .pad {
                make.width.equalToSuperview().multipliedBy(0.4)
            } else {
                make.width.equalToSuperview().multipliedBy(0.5)
            }
            
            make.height.equalToSuperview().multipliedBy(0.5)
            make.top.equalToSuperview().inset(16 + inset.top)
            make.right.equalToSuperview().inset(16 + inset.right)
        }
    }
}
