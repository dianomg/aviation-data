
import UIKit

// MARK: UICollectionView

public extension UICollectionView {

    /// Register a class for use in creating new collection view cells.
    ///
    /// String representation of class is used as identifier
    /// - Parameter cellClass: CellClass to register
    func register<T: UICollectionViewCell>(_ cellClass: T.Type) {
        register(cellClass, forCellWithReuseIdentifier: String(describing: cellClass))
    }

    /// Returns a reusable cell object of speficied type located by its identifier
    ///
    /// If custom cell has not been registered then assertion is occured and new created cell is returned as fallback
    /// - Parameter cellClass: Custom type of cell
    /// - Parameter indexPath: The index path specifying the location of the cell
    func dequeue<T: UICollectionViewCell>(_ cellClass: T.Type, indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: String(describing: cellClass), for: indexPath) as? T else {
            fatalError("Cell is not registered")
        }
        return cell
    }
}

// MARK: UITableView

public extension UITableView {

    /// Register a class for use in creating new table view cells.
    ///
    /// String representation of class is used as identifier
    /// - Parameter cellClass: cellClass to register
    func register<T: UITableViewCell>(_ cellClass: T.Type) {
        register(cellClass, forCellReuseIdentifier: String(describing: cellClass))
    }

    /// Register a class as nib for use in creating new table view cells.
    ///
    /// String representation of class is used as identifier
    /// - Parameter cellClass: cellClass to register
    func registerNib<T: UITableViewCell>(_ cellClass: T.Type) {
        let nib = UINib(nibName: String(describing: cellClass), bundle: nil)
        register(nib, forCellReuseIdentifier: String(describing: cellClass))
    }

    /// Returns a reusable cell object of speficied type located by its identifier
    ///
    /// If custom cell has not been registered then assertion is occured and new created cell is returned as fallback
    /// - Parameter cellClass: Custom type of cell
    /// - Parameter indexPath: The index path specifying the location of the cell
    func dequeue<T: UITableViewCell>(_ cellClass: T.Type, indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: String(describing: cellClass), for: indexPath) as? T else {
            fatalError("Cell is not registered")
        }
        return cell
    }
}
