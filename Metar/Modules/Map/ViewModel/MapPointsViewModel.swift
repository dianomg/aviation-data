import Foundation
import UIKit

class MapPointsViewModel {
    
    enum ViewEvent {
        case append
        case reload
        case update
        case speed
    }
    
    private let circleSettings: MapCircleSettings
    
    var dataItems: [MetarData] = [] {
        didSet {
            items.forEach { $0.dataItems = dataItems }
        }
    }
    
    private(set) var items: [MapPointItem] = []
    
    var viewModelChanged: ((ViewEvent) -> Void)?
    
    var title = "YOUR ROUTE"
    
    var totalTimeAttributed: NSAttributedString {
        let result = NSMutableAttributedString(string: "Total Time: ", attributes: [.foregroundColor: Colors.textGray, .font: UIFont.systemFont(ofSize: 15)])
        result.append(NSAttributedString(string: totalTime, attributes: [.foregroundColor: Colors.white, .font: UIFont.systemFont(ofSize: 15)]))
        return result
    }
    private var totalTime: String {
        var result: Double = 0
        for item in items {
            result += item.time
        }
        return result.getTimeText()
    }
    
    var averageGroundSpeedTitle: String { "Average Ground Speed:" }
    var averageGroundSpeed: String {
        return speed.getSpeedText()
    }
    
    var totalDistanceAttributed: NSAttributedString {
        let result = NSMutableAttributedString(string: "Total Distance: ", attributes: [.foregroundColor: Colors.textGray, .font: UIFont.systemFont(ofSize: 15)])
        result.append(NSAttributedString(string: totalDistance, attributes: [.foregroundColor: Colors.white, .font: UIFont.systemFont(ofSize: 15)]))
        return result
    }
    private var totalDistance: String {
        var result: Double = 0
        for item in items {
            result += item.distance
        }
        return result.getDistanceText()
    }
    private var speed: Double {
        let diff = maxSpeed - minSpeed
        let speed = minSpeed + diff * speedPercent
        return speed
    }
    
    private let minSpeed: Double = Measurement(value: 10, unit: UnitSpeed.knots).converted(to: UnitSpeed.metersPerSecond).value
    private let maxSpeed: Double = Measurement(value: 200, unit: UnitSpeed.knots).converted(to: UnitSpeed.metersPerSecond).value
    private(set) var speedPercent: Double = 0.5
    
    init(circleSettings: MapCircleSettings) {
        self.circleSettings = circleSettings
    }
    
    func update() {
        items.forEach { $0.update() }
    }
    
    func addPoint(_ coordinates: Coordinates) {
        let point = MapPointItem(coordinates: coordinates, circleSettings: circleSettings)
        point.dataItems = dataItems
        items.append(point)
        
        updateItems()
        
        viewModelChanged?(.append)
    }
    
    func addPoint(_ coordinates: Coordinates, dataItem: MetarData) {
        let point = MapPointItem(coordinates: coordinates, circleSettings: circleSettings)
//        point.dataItems = dataItems
        items.append(point)
        
        updateItems()
        
        viewModelChanged?(.append)
    }
    
    func removePoint(_ coordinates: Coordinates) {
        items.removeAll(where: { $0.coordinates == coordinates })
        
        updateItems()
        
        viewModelChanged?(.reload)
    }
    
    func movePoint(_ coordinates: Coordinates) {
        updateItems()
        viewModelChanged?(.update)
    }
    
    func didChangeOrder(from: Int, to: Int) {
        let moved = items[from]
        items[from] = items[to]
        items[to] = moved
        updateItems()
        viewModelChanged?(.reload)
    }
    
    private func updateItems() {
        items.forEach { $0.nextItem = nil }
        for i in 0..<items.count {
            items[i].number = i + 1
            if i < items.count - 1 {
                items[i].nextItem = items[i + 1]
            }
            items[i].speed = speed
        }
    }
}

extension MapPointsViewModel: MapPointsViewDelegate {
    func sliderValueChanged(slider: UISlider) {
        speedPercent = Double(slider.value)
        updateItems()
        viewModelChanged?(.speed)
    }
}
