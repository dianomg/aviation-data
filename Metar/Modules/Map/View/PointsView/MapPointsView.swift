import Foundation
import UIKit

protocol MapPointsViewDelegate: class {
    func sliderValueChanged(slider: UISlider)
}

final class MapPointsView: UIView {
    
    private let titleLabel = UILabel.label(font: .systemFont(ofSize: 15), color: Colors.white)
    private let tableView = UITableView()
    private let adapter = MapPointsAdapter()
    
    private let statusView = PoitsStatusView()
    private let slider = SliderView()
    
    weak var delegate: MapPointsViewDelegate?
    
    init() {
        super.init(frame: .zero)
        createViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        createViews()
    }
    
    func setup(viewModel: MapViewModel, event: MapPointsViewModel.ViewEvent) {
        adapter.viewModel = viewModel
        delegate = viewModel.pointsVm
        
        switch event {
        case .reload:
            tableView.reloadData()
        case .append:
            tableView.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                self.tableView.scrollToRow(at: IndexPath(row: self.adapter.items.count - 1, section: 0), at: .bottom, animated: true)
            }
        case .update:
            break
        case .speed:
            break
//            tableView.reloadData()
        }
        statusView.setup(viewModel: viewModel.pointsVm)
        titleLabel.text = viewModel.pointsVm.title
        slider.setValue(Float(viewModel.pointsVm.speedPercent), animated: false)
    }
    
    private func createViews() {
        backgroundColor = .black
        tableView.separatorStyle = .none
        tableView.backgroundColor = .black
        tableView.allowsSelectionDuringEditing = true
        
        addSubview(titleLabel)
        addSubview(tableView)
        addSubview(statusView)
        addSubview(slider)
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(8)
            make.left.equalToSuperview().inset(16)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.left.right.equalToSuperview()
        }
        
        statusView.snp.makeConstraints { (make) in
            make.top.equalTo(tableView.snp.bottom).offset(4)
            make.left.right.equalToSuperview()
        }
        
        slider.snp.makeConstraints { (make) in
            make.top.equalTo(statusView.snp.bottom)
            make.left.right.equalToSuperview().inset(16)
            make.height.equalTo(50)
            make.bottom.equalToSuperview()
        }
        
        tableView.register(MapPointsCell.self)
        tableView.dataSource = adapter
        tableView.delegate = adapter
        tableView.isEditing = true
        
        layer.borderWidth = 1
        layer.borderColor = UIColor.black.cgColor
        layer.cornerRadius = 6
        layer.masksToBounds = true
        
        slider.addTarget(self, action: #selector(onValueChanged), for: .valueChanged)
    }
    
    func update(mapMode: MapMode, animated: Bool) {
        if mapMode == .route {
            show(animated: animated)
        } else {
            hide(animated: animated)
        }
    }
    
    private func show(animated: Bool) {
        guard animated else { alpha = 1.0; return }
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1.0
        }
    }
    
    private func hide(animated: Bool) {
        guard animated else { alpha = 0.0; return }
        UIView.animate(withDuration: 0.3) {
            self.alpha = 0.0
        }
    }
    
    @objc
    private func onValueChanged() {
        delegate?.sliderValueChanged(slider: slider)
    }
}

final class PoitsStatusView: UIView {
    
    private let totalDistanceLabel = UILabel.label(font: .systemFont(ofSize: 14), color: Colors.textGray)
    private let totalTimeLabel = UILabel.label(font: .systemFont(ofSize: 14), color: Colors.textGray)
    private let separator = UIView()
    private let averageGroundSpeedLabel = UILabel.label(font: .systemFont(ofSize: 14), color: Colors.textGray)
    private let averageGroundSpeedValueLabel = UILabel.label(font: .systemFont(ofSize: 14), color: Colors.white)
    
    init() {
        super.init(frame: .zero)
        createViews()
    }
    
    func setup(viewModel: MapPointsViewModel) {
        totalDistanceLabel.attributedText = viewModel.totalDistanceAttributed
        totalTimeLabel.attributedText = viewModel.totalTimeAttributed
        averageGroundSpeedLabel.text = viewModel.averageGroundSpeedTitle
        averageGroundSpeedValueLabel.text = viewModel.averageGroundSpeed
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createViews() {
        addSubview(totalDistanceLabel)
        addSubview(totalTimeLabel)
        addSubview(separator)
        addSubview(averageGroundSpeedLabel)
        addSubview(averageGroundSpeedValueLabel)
        
        separator.backgroundColor = Colors.gray
        
        totalDistanceLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(16)
            make.top.equalToSuperview().inset(4)
        }
        
        totalTimeLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(4)
            make.right.equalToSuperview().inset(16)
        }
        
        separator.snp.makeConstraints { (make) in
            make.top.equalTo(totalDistanceLabel.snp.bottom).offset(8)
            make.left.right.equalToSuperview()
            make.height.equalTo(1)
        }
        
        averageGroundSpeedLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(16)
            make.top.equalTo(separator.snp.bottom).offset(8)
            make.bottom.equalToSuperview()
        }
        
        averageGroundSpeedValueLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview().inset(16)
            make.top.equalTo(separator.snp.bottom).offset(8)
        }
    }
}
