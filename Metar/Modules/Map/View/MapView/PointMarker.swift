import Foundation
import UIKit
import GoogleMaps

class PointMarker: GMSMarker {
    
    private let size = CGSize(width: 32, height: 32)
    private let titleSize = CGSize(width: 32, height: 16)
    private let fontSize: CGFloat = 16
    
    private let border: CGFloat = 2
    private let fillColor = Colors.white.withAlphaComponent(0.5)
    private let borderColor = UIColor.white
    private let alpha: CGFloat = 0.5
    private let centerRadius: CGFloat = 2

    init(title: String) {
        super.init()
        
        let pointImage = makePointImage()
        let titleImage = makeTitleImage(title: title)
        
        let extend: CGFloat = 1.4
        let spacing: CGFloat = 4
         
        let viewRect = CGRect(x: 0, y: 0, width: size.width * extend, height: (size.height + titleSize.height + spacing) * extend)
        let offsetX = (viewRect.size.width - size.width) / 2
        let offsetY = (viewRect.size.height - (size.height + titleSize.height + spacing)) / 2
        
        let view = UIView()
        view.frame = viewRect
        
        let pointView = UIImageView(image: pointImage)
        pointView.frame = CGRect(x: offsetX, y: titleSize.height + offsetY + spacing, width: size.width, height: size.height)
        view.addSubview(pointView)
        
        let titleView = UIImageView(image: titleImage)
        titleView.frame = CGRect(x: offsetX, y: offsetY, width: titleSize.width, height: titleSize.height)
        view.addSubview(titleView)
        
        let anchorY = pointView.center.y / viewRect.height
        
        groundAnchor = CGPoint(x: 0.5, y: anchorY)
        
        iconView = view
    }
    
    private func makePointImage() -> UIImage {
        UIGraphicsImageRenderer(size: size).image { ctx in
            ctx.cgContext.saveGState()
            
            ctx.cgContext.setFillColor(fillColor.cgColor)
            ctx.cgContext.setStrokeColor(borderColor.cgColor)
            ctx.cgContext.setLineWidth(border)

            ctx.cgContext.addEllipse(in: .init(x: border, y: border, width: size.width - border * 2, height: size.height -  border * 2))
            ctx.cgContext.drawPath(using: .fillStroke)
            
            ctx.cgContext.restoreGState()
            ctx.cgContext.setFillColor(borderColor.cgColor)
            ctx.cgContext.setLineWidth(0)
            let x: CGFloat = size.width / 2 - centerRadius
            let y: CGFloat = size.height / 2 - centerRadius
            ctx.cgContext.addEllipse(in: .init(x: x, y: y, width: centerRadius * 2, height: centerRadius * 2))
            ctx.cgContext.drawPath(using: .fillStroke)
        }
    }
    
    private func makeTitleImage(title: String) -> UIImage {
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        let attrs: [NSAttributedString.Key: Any] = [
            .foregroundColor: Colors.white,
            .font: UIFont.systemFont(ofSize: fontSize),
            .paragraphStyle: style
        ]
        let attrStr = NSAttributedString(string: title, attributes: attrs)
        UIGraphicsBeginImageContextWithOptions(titleSize, false, UIScreen.main.scale)
        let rect = CGRect(x: 0, y: 0, width: titleSize.width, height: titleSize.height)
        attrStr.draw(in: rect)
        let markerImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return markerImage
    }
}
