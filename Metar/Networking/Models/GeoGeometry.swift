import Foundation

enum GeoGeometry: Codable {
    case point(coordinates: Coordinates)
    case polygon(coordinates: [[Coordinates]])
    
    enum CodingKeys: CodingKey {
        case type
        case coordinates
    }
    
    enum CodingError: Error {
        case unknownValue
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let type = try container.decode(String.self, forKey: .type)
        switch type {
        case "Point":
            let coordinatesArray = try container.decode([Double].self, forKey: .coordinates)
            guard let coordinates = Coordinates(array: coordinatesArray) else { throw CodingError.unknownValue }
            self = .point(coordinates: coordinates)
        case "Polygon":
            let coordinatesArray = try container.decode([[[Double]]].self, forKey: .coordinates)
            let coordinates = coordinatesArray.compactMap { $0.compactMap { Coordinates(array: $0) } }
            self = .polygon(coordinates: coordinates)
        default:
            throw CodingError.unknownValue
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        switch self {
        case .point(let coordinates):
            try container.encode("Point", forKey: .type)
            try container.encode([coordinates.latitude, coordinates.longitude], forKey: .coordinates)
        case .polygon(let coordinates):
            try container.encode("Polygon", forKey: .type)
            try container.encode(coordinates.map { $0.map { [$0.latitude, $0.longitude] } }, forKey: .coordinates)
        }
    }
}
