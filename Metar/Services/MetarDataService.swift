import Foundation
import Alamofire

class MetarDataService {
    
    private let apiKey = ["X-API-Key": Environment.checkWxApiKey]
    private let networkClient = NetworkClient(hostUrl: "https://api.checkwx.com/")
    
    init() {}
    
    func getMetarInformation(icaos: [String], completion: @escaping ([MetarData]?, ApiError?) -> Void) {
        let icaos = icaos.joined(separator: ",")
        let params = RequestParameters(
            path: "metar/\(icaos)/decoded",
            method: .get,
            additionalHeaders: HTTPHeaders(apiKey)
        )
        
        networkClient.requestModel(with: params, modelType: MetarDataList.self) { (model, error) in
            completion(model?.data, error)
        }
    }
    
    func getMetarInformation(latitude: Double, longitude: Double, completion: @escaping ([MetarData]?, ApiError?) -> Void) {
        let params = RequestParameters(
            path: "metar/lat/\(latitude)/lon/\(longitude)/decoded",
            method: .get,
            additionalHeaders: HTTPHeaders(apiKey)
        )
        
        networkClient.requestModel(with: params, modelType: MetarDataList.self) { (model, error) in
            completion(model?.data, error)
        }
    }
    
    func getMetarInformation(latitude: Double, longitude: Double, radius: Double, completion: @escaping ([MetarData]?, ApiError?) -> Void) {
        let params = RequestParameters(
            path: "metar/lat/\(latitude)/lon/\(longitude)/radius/\(radius)/decoded",
            method: .get,
            additionalHeaders: HTTPHeaders(apiKey)
        )
        
        networkClient.requestModel(with: params, modelType: MetarDataList.self) { (model, error) in
            completion(model?.data, error)
        }
    }
}
