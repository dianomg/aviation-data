import Foundation

protocol MapHudViewModelDelegate: class {
    func circleRadiusChanged(percent: Float)
}

class MapHudViewModel {
    
    private(set) var infoVm: MapHudInfoViewModel?
    private(set) var startSliderPercent: Float
    weak var delegate: MapHudViewModelDelegate?
    
    init(startSliderPercent: Float, delegate: MapHudViewModelDelegate? = nil) {
        self.startSliderPercent = startSliderPercent
        self.delegate = delegate
    }
    
    func setup(dataItem: MetarData) {
        infoVm = MapHudInfoViewModel(dataItem: dataItem)
    }
}

extension MapHudViewModel: MapHudViewDelegate {
    func sliderChanged(value: Float) {
        startSliderPercent = value
        delegate?.circleRadiusChanged(percent: value)
    }
}
