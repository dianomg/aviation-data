import Foundation
import UIKit

struct MapCircleItem {
    
    var coordinates: Coordinates { dataItem.point ?? Coordinates() }
    var radius: Double
    
    var color: UIColor {
        guard let visibilityValue = dataItem.visibility?.milesValue else { return .black }
        if dataItem.icao.hasPrefix("K") {
            switch visibilityValue {
            case let value where value < 0:
                return .blue
            case let value where value <= 6:
                return .red
            case let value where value < 8:
                return .orange
            case let value where value < 10:
                return .yellow
            default:
                return .green
            }
        } else {
            switch visibilityValue {
            case let value where value < 0:
                return .blue
            case let value where value <= 3:
                return .red
            case let value where value < 5:
                return .orange
            case let value where value < 6:
                return .yellow
            default:
                return .green
            }
        }
    }
    
    var title: String { dataItem.icao }
    
    private let dataItem: MetarData
    
    init(dataItem: MetarData, radius: Double) {
        self.dataItem = dataItem
        self.radius = radius
    }
}

extension MapCircleItem: Equatable {
    static func == (lhs: MapCircleItem, rhs: MapCircleItem) -> Bool {
        lhs.coordinates == rhs.coordinates
            && lhs.color == rhs.color
            && lhs.radius == rhs.radius
    }
}
