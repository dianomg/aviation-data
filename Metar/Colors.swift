import Foundation
import UIKit

enum Colors {
    static let textGray = UIColor(hex: "#A3A3A3")
    static let textBlack = UIColor.black
    static let black = UIColor.black
    static let gray = UIColor(hex: "#A3A3A3")
    static let white = UIColor.white
}
