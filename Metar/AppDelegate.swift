import UIKit
import GoogleMaps

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey(Environment.googleMapsApiKey)
        
        let appWindow = UIWindow(frame: UIScreen.main.bounds)
        self.window = appWindow
        
        let rootController = MapViewController(viewModel: MapViewModel(dataService: MetarDataService()))
        
        window?.rootViewController = rootController
        window?.makeKeyAndVisible()
        
        return true
    }
}

