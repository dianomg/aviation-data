import Foundation

struct MetarData: Codable {
    
    private enum CodingKeys: String, CodingKey {
        case observed
        case location
        case icao
        case visibility
        case station
        case rawText = "raw_text"
    }
    
    private let location: GeoGeometry

    let observed: String
    let visibility: Visibility?
    let icao: String
    let station: Station
    let rawText: String
    
    var point: Coordinates? {
        if case let .point(coordinates) = location {
            return coordinates
        }
        return nil
    }
}

struct MetarDataList: Codable {
    let results: Int
    let data: [MetarData]
}

extension MetarData {
    struct Visibility: Codable {
        
        struct Vertical: Codable {
            let feet: Float
            let meters: Float
        }
        
        private enum CodingKeys: String, CodingKey {
            case miles
            case milesFloat = "miles_float"
            case meters
            case metersFloat = "meters_float"
            case vertical
        }
        
        private let miles: String?
        private let milesFloat: Float?
        private let meters: String?
        private let metersFloat: Float?
        private let vertical: Vertical?
        
        var metersValue: Float { metersFloat ?? vertical?.meters ?? 0 }
        var milesValue: Float {
            if let milesFloat = milesFloat {
                return milesFloat
            } else {
                let feet = vertical?.feet ?? 0
                return Float(Measurement(value: Double(feet), unit: UnitLength.feet).converted(to: UnitLength.miles).value)
            }
        }
    }
    
    struct Station: Codable {
        let name: String
    }
}
