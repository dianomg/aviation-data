import Foundation
import UIKit

class MapPointsCell: UITableViewCell {
    
    private let containerView = UIView()
    private let infoLabel = UILabel.label(font: .systemFont(ofSize: 12), color: Colors.white, lines: 0)
    private let distanceTimeLabel = UILabel.label(font: .systemFont(ofSize: 12), color: Colors.white, lines: 0)
    private let moveIcon = UIImageView(image: UIImage(named: "more"))
    private var item: MapPointItem?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup(item: MapPointItem) {
        self.item = item
        updateUI()
        item.itemChanged = { [weak self] in
            self?.updateUI()
        }
    }
    
    private func updateUI() {
        guard let item = item else { return }
        infoLabel.text = item.tableInfoText
        distanceTimeLabel.text = item.distanceAndTimeText
    }
    
    private func createViews() {
        backgroundColor = Colors.black
        contentView.backgroundColor = Colors.black
        selectionStyle = .none
        
        contentView.addSubview(containerView)
        
        containerView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalToSuperview().inset(4)
            make.width.equalTo(self.snp.width).offset(-8)
        }
        
        containerView.layer.cornerRadius = 4
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = Colors.gray.cgColor
        
        containerView.addSubview(infoLabel)
    
        infoLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(12)
            make.top.bottom.equalToSuperview().inset(4)
        }
        
        let distanceTimeContainerView = UIView()
        distanceTimeContainerView.isUserInteractionEnabled = false
        distanceTimeContainerView.backgroundColor = Colors.black
        
        let coverView = UIView()
        coverView.backgroundColor = Colors.black
        distanceTimeContainerView.addSubview(coverView)
        coverView.snp.makeConstraints { (make) in
            make.top.right.bottom.equalToSuperview()
            make.width.equalTo(70)
        }
        
        distanceTimeContainerView.addSubview(distanceTimeLabel)
        distanceTimeLabel.snp.makeConstraints { (make) in
            make.centerY.left.equalToSuperview()
        }
        distanceTimeLabel.setContentHuggingPriority(.required, for: .horizontal)
        
        distanceTimeContainerView.addSubview(moveIcon)
        moveIcon.snp.makeConstraints { (make) in
            make.right.equalToSuperview().inset(4)
            make.width.height.equalTo(24)
            make.left.equalTo(distanceTimeLabel.snp.right).offset(2)
        }
        
        containerView.addSubview(distanceTimeContainerView)
        distanceTimeContainerView.snp.makeConstraints { (make) in
            make.right.top.bottom.equalToSuperview()
            make.left.equalTo(infoLabel.snp.right)
            make.width.equalTo(70).priority(.high)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        bringSubviewToFront(contentView)
    }
}
