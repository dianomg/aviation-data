import Foundation
import UIKit

extension UIViewController {
    func showDestructiveAlert(title: String, message: String? = nil, ok: (() -> Void)? = nil, cancel: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .destructive, handler: { action in
              ok?()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { action in
              cancel?()
        })
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }
}
