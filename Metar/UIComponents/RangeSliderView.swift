import Foundation
import UIKit

final class RangeSliderView: UIView {
    
    let slider = SliderView()
    
    init() {
        super.init(frame: .zero)
        createViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        createViews()
    }
    
    private func createViews() {
        
        slider.isContinuous = false
        
        addSubview(slider)
        
        slider.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.height.equalTo(50)
        }
    }
}
