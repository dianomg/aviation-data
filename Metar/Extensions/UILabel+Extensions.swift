import Foundation
import UIKit

extension UILabel {
    static func label(font: UIFont,
                      title: String? = nil,
                      color: UIColor = .black,
                      alignment: NSTextAlignment = .left,
                      lines: Int = 1) -> UILabel {
        let label = UILabel()
        label.font = font
        label.text = title
        label.textColor = color
        label.textAlignment = alignment
        label.numberOfLines = lines
        return label
    }
}
