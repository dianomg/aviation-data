import Foundation
import UIKit

final class SwitchView: UIView {
    
    private lazy var segmentControl: UISegmentedControl = {
        let segmentedControl = UISegmentedControl(items: ["Weather", "Route"])
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.addTarget(self, action: #selector(onSwitch), for: .valueChanged)
        segmentedControl.tintColor = .black
        if #available(iOS 13, *) {
            segmentedControl.selectedSegmentTintColor = Colors.white
            segmentedControl.setTitleTextAttributes([.foregroundColor: Colors.textBlack,
                                                     .font: UIFont.systemFont(ofSize: 18)], for: .selected)
            segmentedControl.setTitleTextAttributes([.foregroundColor: Colors.textGray,
                                                     .font: UIFont.systemFont(ofSize: 18)], for: .normal)
        }
        return segmentedControl
    }()
    
    var switched: (() -> Void)?
    
    init() {
        super.init(frame: .zero)
        createViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        createViews()
    }
    
    private func createViews() {
        addSubview(segmentControl)
        
        segmentControl.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(1)
        }
        
        layer.cornerRadius = 8
        backgroundColor = Colors.black
    }
    
    @objc
    private func onSwitch() {
        switched?()
    }
}
