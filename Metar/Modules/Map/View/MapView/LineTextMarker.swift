import Foundation
import UIKit
import GoogleMaps

class LineTextMarker: GMSMarker {
    
    private let inset: UIEdgeInsets = .init(top: 4, left: 12, bottom: 4, right: 12)

    init(text: String) {
        super.init()
        
        let containerView = UIView()
        containerView.backgroundColor = Colors.white
        containerView.layer.cornerRadius = 4
        
        let label = UILabel.label(font: .boldSystemFont(ofSize: 12), color: .black)
        label.text = text
        label.sizeToFit()
        
        containerView.frame = label.frame.insetBy(dx: -(inset.left + inset.right) / 2, dy: -(inset.top + inset.bottom) / 2)
        containerView.addSubview(label)
        
        label.frame = CGRect(x: inset.left, y: inset.top, width: label.frame.width, height: label.frame.height)
        
        iconView = containerView
    }
}
