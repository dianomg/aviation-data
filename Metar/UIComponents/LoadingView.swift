import Foundation
import UIKit

final class LoadingView: UIView {
    
    private let activityIndicator = UIActivityIndicatorView(style: .white)
    private let label = UILabel.label(font: .systemFont(ofSize: 14), color: Colors.white)
    
    private let text: String
    var isLoading = false
    
    init(text: String) {
        self.text = text
        super.init(frame: .zero)
        createViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createViews() {
        addSubview(activityIndicator)
        addSubview(label)
        
        activityIndicator.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(8)
            make.centerY.equalToSuperview()
        }
        
        label.snp.makeConstraints { (make) in
            make.left.equalTo(activityIndicator.snp.right).offset(4)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().inset(8)
            make.top.equalToSuperview().inset(8)
        }
        
        backgroundColor = .black
        layer.cornerRadius = 4
        
        label.text = text
    }
    
    func show() {
        guard !isLoading else { return }
        self.isLoading = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            guard self.isLoading else { return }
            self.activityIndicator.startAnimating()
            UIView.animate(withDuration: 0.15) {
                self.alpha = 1.0
            }
        }
    }
    
    func hide() {
        isLoading = false
        activityIndicator.stopAnimating()
        UIView.animate(withDuration: 0.15) {
            self.alpha = 0.0
        }
    }
}
