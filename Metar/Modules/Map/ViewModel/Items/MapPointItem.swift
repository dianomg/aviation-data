import UIKit

class MapPointItem {
    
    private let circleSettings: MapCircleSettings
    
    private let formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.minimumIntegerDigits = 2
        return formatter
    }()
    
    private let coordinatesFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 4
        formatter.minimumFractionDigits = 4
        return formatter
    }()
    
    var dataItems: [MetarData] = [] {
        didSet {
            update()
        }
    }
    
    private var dataItem: MetarData?
    
    var number: Int = 0 {
        didSet {
            update()
        }
    }
    var title: String { dataItem?.icao ?? "WPT" }
    var coordinates: Coordinates {
        didSet {
            update()
        }
    }
    
    weak var nextItem: MapPointItem? {
        didSet {
            update()
        }
    }
    
    var speed: Double = 0 {
        didSet {
            updateDistanceAndTime()
        }
    }
    
    var time: Double { distance / speed }
    
    private(set) var pointTitleText: String = ""
    private(set) var tableInfoText: String = ""
    private(set) var distanceText: String = ""
    private(set) var distanceAndTimeText: String = ""
    
    var distance: Double {
        guard let next = nextItem else { return 0 }
        return coordinates.location.distance(from: next.coordinates.location)
    }
    
    var itemChanged: (() -> Void)?
    
    init(coordinates: Coordinates, circleSettings: MapCircleSettings) {
        self.coordinates = coordinates
        self.circleSettings = circleSettings
        update()
    }
    
    func update() {
        pointTitleText = makeTitle()
        dataItem = findDataItem(by: coordinates)
        distanceText = distance > 0 ? distance.getDistanceText() : ""
        updateDistanceAndTime()
        tableInfoText = makeTableInfoText()
        itemChanged?()
    }
    
    private func updateDistanceAndTime() {
        distanceAndTimeText = distance > 0 ? distance.getDistanceText() + " " + time.getTimeText() : ""
        itemChanged?()
    }
    
    private func makeTableInfoText() -> String {
        let number = makeTitle()
        let lat = coordinatesFormatter.string(from: NSNumber(value: coordinates.latitude)) ?? ""
        let lon = coordinatesFormatter.string(from: NSNumber(value: coordinates.longitude)) ?? ""
        let coordinates = "\(lat)/\(lon)"
        return "\(number). \(title) \(coordinates)"
    }
    
    private func makeTitle() -> String {
        formatter.string(from: NSNumber(value: number)) ?? ""
    }
    
    private func findDataItem(by coordinates: Coordinates) -> MetarData? {
        for item in dataItems {
            if let point = item.point, point.location.distance(from: coordinates.location) <= circleSettings.circleRadius {
                return item
            }
        }
        return nil
    }
}

extension Double {
    func getDistanceText() -> String {
        let distanceInNM = Measurement(value: self, unit: UnitLength.meters).converted(to: UnitLength.nauticalMiles).value
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = 1
        let result = formatter.string(from: NSNumber(value: distanceInNM)) ?? ""
        return "\(result)NM"
    }
    
    func getTimeText() -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .abbreviated
        let formattedString = formatter.string(from: TimeInterval(self))!
        return formattedString
    }
    
    func getSpeedText() -> String {
        let speedInKts = Measurement(value: self, unit: UnitSpeed.metersPerSecond).converted(to: UnitSpeed.knots).value
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = 1
        let result = formatter.string(from: NSNumber(value: speedInKts)) ?? ""
        return "\(result)kts"
    }
}
