import Foundation
import UIKit
import SnapKit

protocol MapHudViewDelegate: class {
    func sliderChanged(value: Float)
}

final class MapHudView: UIView {
    
    private let idLabel = UILabel.label(font: .systemFont(ofSize: 21), color: Colors.white)
    private let stationLabel = UILabel.label(font: .systemFont(ofSize: 21), color: Colors.white)
    private let locationLabel = UILabel.label(font: .systemFont(ofSize: 21), color: Colors.white)
    private let visibilityLabel = UILabel.label(font: .systemFont(ofSize: 21), color: Colors.white)
    private let dateLabel = UILabel.label(font: .systemFont(ofSize: 21), color: Colors.white)
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 4
        return stackView
    }()
    
    private let rawLabel = UILabel.label(font: .systemFont(ofSize: 15), color: Colors.textGray)
    
    private let sliderView = RangeSliderView()
    
    weak var delegate: MapHudViewDelegate?
    
    init() {
        super.init(frame: .zero)
        createViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        createViews()
    }
    
    func setup(viewModel: MapHudViewModel) {
        if let vm = viewModel.infoVm {
            idLabel.text = vm.id
            stationLabel.text = vm.station
            locationLabel.text = vm.location
            visibilityLabel.text = vm.visibility
            dateLabel.text = vm.date
            rawLabel.text = vm.rawText
        }
        
        sliderView.slider.setValue(viewModel.startSliderPercent, animated: false)
        
        delegate = viewModel
    }
    
    private func createViews() {
        backgroundColor = .black
        
        stackView.addArrangedSubview(wrapInStack(views: [idLabel, stationLabel, visibilityLabel]))
        stackView.addArrangedSubview(rawLabel)
        
        addSubview(stackView)
        addSubview(sliderView)
        
        setupStackViewConstraints(.zero)
        setupSliderConstraints(.zero)
        sliderView.slider.addTarget(self, action: #selector(onSliderValueChanged), for: .valueChanged)
        
        layer.cornerRadius = 6
    }
    
    func updateSafeAreaInsets(_ insets: UIEdgeInsets) {
        setupSliderConstraints(insets)
        setupStackViewConstraints(insets)
    }
    
    private func setupStackViewConstraints(_ insets: UIEdgeInsets) {
        stackView.snp.remakeConstraints { (make) in
            make.left.equalToSuperview().inset(16 + insets.left)
            make.top.equalToSuperview().inset(4)
            make.bottom.equalToSuperview().inset(4)
            make.right.equalTo(sliderView.snp.left).offset(-16)
        }
    }
    
    private func setupSliderConstraints(_ insets: UIEdgeInsets) {
        sliderView.snp.remakeConstraints { (make) in
            make.right.equalToSuperview().inset(16 + insets.right)
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.33)
        }
    }
    
    @objc
    private func onSliderValueChanged() {
        delegate?.sliderChanged(value: sliderView.slider.value)
    }
    
    private func wrapInStack(views: [UIView]) -> UIView {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.alignment = .leading
        stackView.spacing = 16
        
        views.forEach { stackView.addArrangedSubview($0) }
        return stackView
    }
}
