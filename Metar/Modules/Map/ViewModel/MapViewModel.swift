import Foundation
import UIKit

enum MapMode {
    case route
    case weather
    
    func toggle() -> Self {
        if self == .route {
            return .weather
        } else {
            return .route
        }
    }
}

class MapViewModel {
    
    enum ViewEvent {
        case reload
        case updateCircles
        case updateHud
        case updateMapMode
        case updateLoading
    }
    
    private let dataService: MetarDataService
    private var circleSettings = MapCircleSettings()
    private var items: [String: MetarData] = [:]
    private var dataItems: [MetarData] { Array(items.values) }
    
    weak var controller: UIViewController?
    
    let startCoordinates = Coordinates(latitude: 55.3617609, longitude: -3.4433238)
    
    var circleItems: [MapCircleItem] {
        Array(items.values).map { MapCircleItem(dataItem: $0, radius: circleSettings.circleRadius) }
    }
    
    private(set) var isLoading: Bool = false {
        didSet {
            viewModelChanged?(.updateLoading)
        }
    }
    
    lazy var pointsVm = MapPointsViewModel(circleSettings: circleSettings)
    lazy var hudVm = MapHudViewModel(
        startSliderPercent: 0.5,
        delegate: self
    )
    
    private(set) var mapMode: MapMode = .weather
    
    var viewModelChanged: ((ViewEvent) -> Void)?
    
    init(dataService: MetarDataService) {
        self.dataService = dataService
    }
    
    func loadData() {
        viewModelChanged?(.reload)
    }
    
    func update(coordinates: Coordinates, region: MapRegion) {
        let radius = min(circleSettings.maxRegionRadius, min(region.inMiles.width, region.inMiles.height))
        isLoading = true
        dataService.getMetarInformation(latitude: coordinates.location.coordinate.latitude,
                                        longitude: coordinates.location.coordinate.longitude,
                                        radius: radius) { [weak self] data, error in
            guard let self = self, let data = data else { return }
            data.forEach {
                guard let point = $0.point else { return }
                self.items[point.location.coordinate.storeKey] = $0
            }
            self.pointsVm.dataItems = self.dataItems
            self.isLoading = false
            self.viewModelChanged?(.reload)
        }
    }
    
    func circleSelected(coordinates: Coordinates) {
        guard let dataItem = items[coordinates.location.coordinate.storeKey] else { return }
        hudVm.setup(dataItem: dataItem)
        viewModelChanged?(.updateHud)
    }
    
    func addPoint(_ coordinates: Coordinates) {
        pointsVm.addPoint(coordinates)
    }
    
    func removePoint(_ coordinates: Coordinates) {
        controller?.showDestructiveAlert(title: "Remove waypoint?", ok: { [weak self] in
            self?.pointsVm.removePoint(coordinates)
        })
    }
    
    func movePoint(_ coordinates: Coordinates) {
        pointsVm.movePoint(coordinates)
    }
    
    func getLengthText(from: Coordinates, to: Coordinates) -> String {
        let distanceInMeters = from.location.distance(from: to.location)
        let distanceInNM = Measurement(value: distanceInMeters, unit: UnitLength.meters).converted(to: UnitLength.nauticalMiles).value
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = 1
        let distance = formatter.string(from: NSNumber(value: distanceInNM)) ?? ""
        return "\(distance)NM"
    }
    
    func mapModeSwitched() {
        mapMode = mapMode.toggle()
        viewModelChanged?(.updateMapMode)
    }
}

extension MapViewModel: MapHudViewModelDelegate {
    func circleRadiusChanged(percent: Float) {
        circleSettings.circleRadiusPercent = percent
        pointsVm.update()
        viewModelChanged?(.updateCircles)
    }
}
