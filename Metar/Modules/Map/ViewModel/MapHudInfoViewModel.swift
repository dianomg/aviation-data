import Foundation

class MapHudInfoViewModel {
    private let dataItem: MetarData
    
    private var visibilityInKm: Double? {
        guard let visibility = dataItem.visibility else { return nil }
        return Measurement(value: Double(visibility.metersValue), unit: UnitLength.meters)
            .converted(to: UnitLength.kilometers).value
    }
    
    var id: String { "\(dataItem.icao)" }
    var location: String {
        guard let point = dataItem.point else { return "" }
        return "Location: Lat: \(point.latitude), Lon: \(point.longitude)"
    }
    var station: String { "\(dataItem.station.name)" }
    var visibility: String {
        if let visibilityInKm = visibilityInKm {
            return "Visibility KM \(visibilityInKm)"
        } else {
            return "Visibility KM N/A"
        }
    }
    var date: String { "Date: \(dataItem.observed)" }
    
    var rawText: String { dataItem.rawText }
    
    init(dataItem: MetarData) {
        self.dataItem = dataItem
    }
}
