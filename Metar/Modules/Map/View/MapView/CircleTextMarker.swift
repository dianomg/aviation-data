import Foundation
import UIKit
import GoogleMaps

final class CircleTextMarker: GMSMarker {
    
    private let inset: CGFloat = 2
    
    init(text: String) {
        super.init()
        
        let containerView = UIView()
        containerView.backgroundColor = .black
        containerView.layer.cornerRadius = 2
        
        let label = UILabel.label(font: .boldSystemFont(ofSize: 10), color: Colors.white)
        label.text = text
        label.sizeToFit()
        
        containerView.frame = label.frame.insetBy(dx: -inset, dy: -inset)
        containerView.addSubview(label)
        
        label.frame = CGRect(x: inset, y: inset, width: label.frame.width, height: label.frame.height)
        
        iconView = containerView
    }
}
