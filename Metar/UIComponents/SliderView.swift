import Foundation
import UIKit

class SliderView: UISlider {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tintColor = Colors.gray
        setThumbImage(UIImage(named: "sliderThumb"), for: .normal)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let point = CGPoint(x: bounds.minX, y: bounds.midY - 3)
        return CGRect(origin: point, size: CGSize(width: bounds.width, height: 6))
    }
}
