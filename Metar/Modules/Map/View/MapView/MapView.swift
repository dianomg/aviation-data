import Foundation
import UIKit
import SnapKit
import GoogleMaps
import MapKit

final class MapView: UIView {
    
    private enum ViewOrder: Int32 {
        case circles
        case circleTexts
        case lines
        case lineTexts
        case points
    }
    
    private let viewModel: MapViewModel
    
    private var circles: [GMSCircle] = []
    private var points: [GMSMarker] = []
    private let path = GMSMutablePath()
    private var line: GMSPolyline?
    private var lineMarkers: [LineTextMarker] = []
    private var circleMarkers: [String: CircleTextMarker] = [:]
    
    private let pointIconView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "routePoint"))
        view.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        return view
    }()
    
    private let camera: GMSCameraPosition
    private lazy var mapView = GMSMapView.map(withFrame: .zero, camera: camera)
    
    private let generator = UIImpactFeedbackGenerator()
    
    init(viewModel: MapViewModel) {
        self.viewModel = viewModel
        self.camera = GMSCameraPosition.camera(withLatitude: viewModel.startCoordinates.latitude,
                                               longitude: viewModel.startCoordinates.longitude,
                                               zoom: 6.0)
        super.init(frame: .zero)
        createViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createViews() {
        addSubview(mapView)
        
        mapView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        mapView.delegate = self
        mapView.mapType = .satellite
        
        generator.prepare()
    }
    
    func updateCircles(items: [MapCircleItem]) {
        let circlesToRemove = circles.filter {
            guard let dataItem = $0.userData as? MapCircleItem else { return false }
            return !items.contains(dataItem)
        }
        circlesToRemove.forEach {
            $0.map = nil
            guard let dataItem = $0.userData as? MapCircleItem else { return }
            circleMarkers[dataItem.coordinates.location.coordinate.storeKey]?.map = nil
        }
        circles = Array(Set(circles).subtracting(circlesToRemove))
        let itemsToAdd = items.filter {
            !circles.compactMap { $0.userData as? MapCircleItem }.contains($0)
        }
        itemsToAdd.forEach {
            let circle = makeCircle(from: $0)
            let text = makeCircleText(from: $0)
            circleMarkers[circle.position.storeKey] = text
            circles.append(circle)
        }
    }
    
    func updatePoints(items: [MapPointItem]) {
        points.forEach { $0.map = nil }
        points.removeAll()
        
        items.forEach {
            let point = makePoint(from: $0)
            points.append(point)
        }
        
        updateLine()
        updateLineMarkers()
    }
    
    private func makeCircle(from item: MapCircleItem) -> GMSCircle {
        let circle = GMSCircle(position: item.coordinates.location.coordinate, radius: item.radius)
        circle.fillColor = item.color.withAlphaComponent(0.5)
        circle.strokeColor = .clear
        circle.map = mapView
        circle.userData = item
        circle.isTappable = true
        circle.zIndex = ViewOrder.circles.rawValue
        return circle
    }
    
    private func makePoint(from item: MapPointItem) -> PointMarker {
        let point = PointMarker(title: item.pointTitleText)
        point.position = item.coordinates.location.coordinate
        point.map = mapView
        point.isDraggable = true
        point.userData = item
        point.zIndex = ViewOrder.points.rawValue
        return point
    }
    
    private func makeCircleText(from item: MapCircleItem) -> CircleTextMarker {
        let marker = CircleTextMarker(text: item.title)
        marker.position = item.coordinates.location.coordinate
        marker.map = mapView
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.0)
        marker.zIndex = ViewOrder.circleTexts.rawValue
        return marker
    }
    
    private func updateLine() {
        line?.map = nil
        path.removeAllCoordinates()
        points.forEach {
            path.add($0.position)
        }
        let line = GMSPolyline(path: path)
        line.map = mapView
        line.strokeColor = .red
        line.strokeWidth = 2
        line.spans = GMSStyleSpans(path, [GMSStrokeStyle.solidColor(Colors.white), GMSStrokeStyle.solidColor(.clear)], [2000, 2000], .rhumb)
        line.zIndex = ViewOrder.lines.rawValue
        self.line = line
    }
    
    private func removeLineMarkers() {
        lineMarkers.forEach { $0.map = nil }
        lineMarkers.removeAll()
    }
    
    private func updateLineMarkers() {
        lineMarkers.forEach { $0.map = nil }
        lineMarkers.removeAll()
        guard points.count > 1 else { return }
        for i in 0 ..< points.count - 1 {
            let from = points[i]
            let to = points[i + 1]
            guard let fromPointItem = from.userData as? MapPointItem else { continue }
            let marker = LineTextMarker(text: fromPointItem.distanceText)
            marker.position = from.position.middleLocationWith(location: to.position)
            marker.map = mapView
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.zIndex = ViewOrder.lineTexts.rawValue
            lineMarkers.append(marker)
        }
    }
}

extension MapView: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let center = position.target.coordinates
        let region = getCurrentMapRegion(position: position)
        viewModel.update(coordinates: center, region: region)
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        guard let item = overlay.userData as? MapCircleItem else { return }
        viewModel.circleSelected(coordinates: item.coordinates)
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        viewModel.addPoint(coordinate.coordinates)
        generator.impactOccurred()
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        removeLineMarkers()
        generator.impactOccurred()
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        updateLine()
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        if let pointItem = marker.userData as? MapPointItem {
            pointItem.coordinates = marker.position.coordinates
        }
        viewModel.movePoint(marker.position.coordinates)
        updateLineMarkers()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let pointItem = marker.userData as? MapPointItem {
            viewModel.removePoint(pointItem.coordinates)
            return true
        }
        return false
    }
}

extension MapView {
    private func getCurrentMapRegion(position: GMSCameraPosition) -> MapRegion {
        let bounds = GMSCoordinateBounds(region: mapView.projection.visibleRegion())
        let latitudeDelta = bounds.northEast.latitude - bounds.southWest.latitude
        let longitudeDelta = bounds.northEast.longitude - bounds.southWest.longitude
        
        let regionCenter = CLLocationCoordinate2DMake(
            (bounds.southWest.latitude + bounds.northEast.latitude) / 2,
            (bounds.southWest.longitude + bounds.northEast.longitude) / 2)
        let span = MKCoordinateSpan(latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta)
        let mapRegion = MKCoordinateRegion(center: regionCenter, span: span)
        
        let region = MapRegion(region: mapRegion)
        return region
    }
}
