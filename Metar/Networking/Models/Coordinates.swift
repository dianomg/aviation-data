import Foundation
import CoreLocation

struct Coordinates: Codable {
    let latitude: Double
    let longitude: Double
    
    init?(array: [Double]) {
        guard
            let latitude = array.last,
            let longitude = array.first
        else { return nil }
        self.latitude = latitude
        self.longitude = longitude
    }
    
    init(latitude: Double = 0, longitude: Double = 0) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    init?(_ coordinates: CLLocationCoordinate2D?) {
        guard let coordinates = coordinates else { return nil }
        self.latitude = coordinates.latitude
        self.longitude = coordinates.longitude
    }
}

extension Coordinates: Equatable {}
extension Coordinates: Hashable {}

extension Coordinates {
    var location: CLLocation { CLLocation(latitude: latitude, longitude: longitude) }
}

extension CLLocationCoordinate2D {
    var coordinates: Coordinates { Coordinates(latitude: latitude, longitude: longitude) }
}
