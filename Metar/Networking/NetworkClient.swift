import Foundation
import Alamofire

class NetworkClient {
    private let hostUrl: String
    private let defaultSession = Session.default
    private let decoder = JSONDecoder()
    
    init(hostUrl: String) {
        self.hostUrl = hostUrl
    }
    
    func requestModel<T: Codable>(with data: RequestParameters,
                                            modelType: T.Type,
                                            completion: @escaping (T?, ApiError?) -> Void) {
        requestData(with: data) { [weak self] (data, error) in
            guard let data = data else {
                DispatchQueue.main.async { completion(nil, error ?? ApiError.dataParsingError()) }
                return
            }
            
            do {
                let model = try self?.decoder.decode(modelType, from: data)
                DispatchQueue.main.async { completion(model, error) }
            } catch(let parsingError) {
                DispatchQueue.main.async { completion(nil, error ?? ApiError.dataParsingError(message: parsingError.localizedDescription)) }
            }
        }
    }
    
    func requestData(with data: RequestParameters, completion: @escaping (Data?, ApiError?) -> Void) {
        request(with: data, session: defaultSession, completion: completion)
    }
    
    private func request(with data: RequestParameters,
                          session: Session,
                          completion: @escaping (Data?, ApiError?) -> Void) {
        let urlOptional = URL(string: hostUrl + data.path)?
            .URLByAppendingQueryParameters(parameters: data.query)
        guard let url = urlOptional else { return }
        var headers = getHeaders()
        if let overridedHeaders = data.additionalHeaders {
            overridedHeaders.forEach { header in
                headers[header.name] = header.value
            }
        }
        
        guard let urlRequest = try? URLRequest(url: url, method: data.method, headers: headers),
              var encodedURLRequest = try? JSONEncoding().encode(urlRequest, with: data.body) else {
            completion(nil, ApiError.wrongUrlError(params: data))
            return
        }
        
        encodedURLRequest.timeoutInterval = data.timeoutInterval
        session.request(encodedURLRequest).responseData(params: data) { [weak self] data, error in
            guard let sSelf = self else { return }
            if let _ = error {
                sSelf.cancelAllRequests()
            }
            completion(data, error)
        }
    }
    
    private func getHeaders() -> HTTPHeaders {
        return HTTPHeaders()
    }
    
    internal func cancelAllRequests() {
        defaultSession.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
    }
}

extension URL {
    func URLByAppendingQueryParameters(parameters: [String: Any]?) -> URL {
        guard let parameters = parameters,
            let urlComponents = NSURLComponents(url: self, resolvingAgainstBaseURL: true) else {
                return self
        }
        
        var queryItems = [URLQueryItem]()
        for param in parameters {
            if let array = param.value as? Array<Any> {
                array.forEach { (v) in
                    queryItems.append(URLQueryItem(name: param.key + "[]", value: "\(v)"))
                }
            } else {
                queryItems.append(URLQueryItem(name: param.key, value: "\(param.value)"))
            }
        }
        urlComponents.queryItems = queryItems
//        urlComponents.queryItems = parameters.map { URLQueryItem(name: $0, value: "\($1)") }
        // escape "+" due to: "https://www.djackson.org/why-we-do-not-use-urlcomponents"
        urlComponents.percentEncodedQuery = urlComponents.percentEncodedQuery?
            // manually encode + into percent encoding
            .replacingOccurrences(of: "+", with: "%2B")
            // optional, probably unnecessary: convert percent-encoded spaces into +
            .replacingOccurrences(of: "%20", with: "+")
        
        
        return urlComponents.url ?? self
    }
}

extension DataRequest {
    @discardableResult
    func responseData(queue: DispatchQueue = DispatchQueue.global(qos: .utility),
                      params: RequestParameters,
                      completion: @escaping (Data?, ApiError?) -> Void) -> DataRequest {
        return validate(statusCode: 200..<400)
            .response(queue: queue, completionHandler: { (response) in
                guard response.error != nil else {
                    completion(response.data, nil)
                    return
                }
                
                // we don't have statusCode - very strange,
                // that means that something bad with network communication
                guard let statusCode = response.response?.statusCode else {
                    completion(nil, ApiError.noServerConnectionError(params: params))
                    return
                }
                
                // if we have data, than we can parse error from response
                if let data = response.data {
                    completion(nil, ApiError(data: data, params: params, code: statusCode))
                    return
                }
                
                // another strange case - we have status code,
                // but don't have error message from server
                switch statusCode {
                case 401:
                    let error = ApiError(type: .unauthenticated,
                                         requestParams: params,
                                         message: "youAreNotLoggedIn",
                                         code: statusCode)
                    completion(nil, error)
                case 403:
                    let error = ApiError(type: .accessTokenExpired,
                                         requestParams: params,
                                         message: "yourSessionExpired",
                                         code: statusCode)
                    completion(nil, error)
                case 426:
                    let error = ApiError(type: .unsupportedVersion,
                                         requestParams: params,
                                         message: "currentVersionIsUnsupported",
                                         code: statusCode)
                    completion(nil, error)
                default:
                    completion(nil, ApiError.unknownServerError(params: params))
                }
            })
    }
}

enum ApiErrorType: String {
    case unauthenticated
    case accessTokenExpired
    case unsupportedVersion
    
    // custom errors
    case wrongUrl
    case dataParsing
    case noConnection
    case unknown
    
    init(from: Int) {
        switch from {
        case 401:
            self = .unauthenticated
        default:
            self = .unknown
        }
    }
}

class ApiError: Error {
    let code: Int
    let requestParams: RequestParameters?
    let message: String
    let type: ApiErrorType
    
    init(type: ApiErrorType,
         requestParams: RequestParameters? = nil,
         message: String = "",
         data: Data? = nil,
         code: Int = 0) {
        self.type = type
        self.requestParams = requestParams
        self.message = message
        self.code = code
    }
    
    init(data: Data, params: RequestParameters?, code: Int) {
        self.requestParams = params
        self.code = code
        self.type = ApiErrorType(from: code)
        self.message = type.rawValue
    }
}

extension ApiError {
    static func unknownServerError(params: RequestParameters?) -> ApiError {
        return ApiError(type: .unknown, requestParams: params)
    }
    
    static func wrongUrlError(params: RequestParameters?) -> ApiError {
        return ApiError(type: .wrongUrl, requestParams: params)
    }
    
    static func dataParsingError(message: String? = nil) -> ApiError {
        return ApiError(type: .dataParsing, message: message ?? "error parsing response JSON")
    }
    
    static func noServerConnectionError(params: RequestParameters?) -> ApiError {
        return ApiError(type: .noConnection,
                        requestParams: params,
                        message: "pleaseCheckYourInternet")
    }
}

struct RequestParameters {
    let path: String
    let method: HTTPMethod
    let query: [String: Any]?
    let body: [String: Any]?
    let additionalHeaders: HTTPHeaders?
    let timeoutInterval: TimeInterval
    
    init(path: String,
         method: HTTPMethod = .get,
         query: [String: Any]? = nil,
         body: [String: Any]? = nil,
         additionalHeaders: HTTPHeaders? = nil,
         timeoutInterval: TimeInterval = 60) {
        self.path = path
        self.method = method
        self.query = query
        self.body = body
        self.additionalHeaders = additionalHeaders
        self.timeoutInterval = timeoutInterval
    }
}
