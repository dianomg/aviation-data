import Foundation

class MapCircleSettings {
    let minCircleRadius = Measurement(value: 5, unit: UnitLength.nauticalMiles).converted(to: UnitLength.meters).value
    let maxCircleRadius = Measurement(value: 15, unit: UnitLength.nauticalMiles).converted(to: UnitLength.meters).value
    let maxRegionRadius: Double = 200
    
    var circleRadiusPercent: Float = 0.5
    
    var circleRadius: Double { minCircleRadius + (maxCircleRadius - minCircleRadius) * Double(circleRadiusPercent) }
}
